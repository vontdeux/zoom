define(['common'], function (angularAMD) {
  'use strict';
  var app = angular.module('angularAmdSample', ['ui.router','ngResource','ngSanitize','mgcrea.ngStrap','ngGrid','template-override','angular-loading-bar', 'ngAnimate']);

  app.config(['$stateProvider', '$urlRouterProvider','$datepickerProvider', function ($stateProvider, $urlRouterProvider,$datepickerProvider) {

    $stateProvider
    .state('landing', angularAMD.route({
      url: '/landing',
      views: {
        'landing' : angularAMD.route({
          templateUrl: 'views/landing.html',
          controllerUrl: 'controllers/landing_ctrl'
        })
      }
    }))
    .state('dashboard', angularAMD.route({
      url: '/dashboard',
      views: {
        'content' : angularAMD.route({
         templateUrl: 'views/dashboard.html',
          controllerUrl: 'controllers/dashboard_ctrl'
        })
      }
    }))
    ;

    // Else
    $urlRouterProvider
    .otherwise('/landing');
      
    angular.extend($datepickerProvider.defaults, {
    dateFormat: 'dd/MM/yyyy',
    startWeek: 1
    });

  }]);
  
return angularAMD.bootstrap(app);
});
