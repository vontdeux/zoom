define(['angularAMD'], function (angularAMD) {
  'use strict';
  angularAMD.controller('navMenuController', ['$scope', '$state','JsonService', function ($scope, $state, JsonService) {

    $scope.layoutValue = 'column';
    $scope.userActivity = [
      {
        "text": "<i class=\"marR5 icon-history\"></i>Recent Login Activity",
        "href": "#anotherAction"
      },
      {
          "text": "<i class=\"marR5 icon-notification\"></i>View All Notification",
          "href": "#"
      },
      {
          "text": "<i class=\"marR5 icon-disk\"></i>My Saves",
          "href": "#"
      },
      {
        "divider": true
      },
      {
          "text": "<i class=\"marR5 icon-sign-out\"></i>Sign Out",
          "click": "$alert(\"Holy guacamole!\")"
      }
    ];


    $scope.ivalue = false;
    $scope.selectedLayout = "default";
    $scope.icons = [{"value":"default","label":"<i class=\"marR5 icon-gear\"></i>Default Layout"},
                    {"value":"horizontal","label":"<i class=\"marR5 icon-arrows-h\"></i> Horizontal Layout"},
                    {"value":"vertical","label":"<i class=\"marR5 icon-arrows-v\"></i> Vertical Layout"}];

    $scope.isMenu = function(str){
      if(typeof str !== 'undefined'){
        var re = /^(\w+)-(\w+)$/; 
        var m = str.match(re);

        var m1 = m[1].substring(0, 1).toUpperCase() + m[1].substring(1).toLowerCase();
        var m2 = m[2].substring(0, 1).toUpperCase() + m[2].substring(1).toLowerCase();

        return m1 + ' '+ m2;
      }
    };

    $scope.isTabActive = function (tabName) {
      if (tabName === $state.current.name) {
        return 'active';
      }
    };

    $scope.getMainMenu = function (menuname) {
        for (var i = 0; i < $scope.menuList.length; i += 1){
          if ($scope.menuList[i].tabName === menuname){
           $scope.currentMenu = $scope.menuList[i].name;
           $scope.iconCurrentMenu = $scope.menuList[i].icon;
           break;
        }
      }
    };

    $scope.getCurrentSubmenu =  function (){
      for (var i = 0; i < $scope.submenuTitleList.length; i += 1){
          if ($scope.submenuTitleList[i].tabName === $state.current.name){
           $scope.currentSubmenuName = $scope.submenuTitleList[i].name;
           break;
        }
      }
    };

    $scope.changeLayout = function (layout){            
      $scope.layoutValue = layout ;      
    };

    $scope.menuList = JsonService.getLeftMenu.getData();
    $scope.submenuTitleList = JsonService.getMenuTitle.getData();
    $scope.localize = JsonService.getHorizontal.getData();

  }]);

  angularAMD.factory('JsonService', function($resource) {
    return {
          getLeftMenu: $resource('/config/left-menu.json', {}, {
          getData: { method: 'GET', params: {}, isArray: true }
        }),
          getMenuTitle: $resource('/config/menu-title.json', {}, {
          getData: { method: 'GET', params: {}, isArray: true }
        }),
          getHorizontal: $resource('/config/horizontal-menu.json', {}, {
          getData: { method: 'GET', params: {}, isArray: false }
        })
      };
  });

  angularAMD.directive('navMain', function () {
    return {
      restrict: 'A',
      controller: 'navMenuController',
      templateUrl: 'views/nav/nav.html'
    };
  });

  angularAMD.directive('toggleLeftSidebar', function () {
    return {
      restrict: 'A',
      template: '<button  ng-click="miniMenu = !miniMenu" class="sidebar-toggle" id="toggle-left"><i class="icon-navicon"></i></button>'        
    };
  });

  angularAMD.directive('toggleClass', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('click', function() {                
          element.parent().toggleClass(attrs.toggleClass);
        });
      }
    };
  });

  

});

