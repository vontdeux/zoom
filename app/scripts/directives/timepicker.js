define(['angularAMD'], function (angularAMD) {
	angularAMD.factory('timepickerState', function() {
	  var pickers = [];
	  return {
			addPicker: function(picker) {
				pickers.push(picker);
			},
			closeAll: function() {
				for (var i=0; i<pickers.length; i++) {
					pickers[i].close();
				}
			}
		};
	})

	angularAMD.directive("timeFormat", function($filter) {
	  return {
	    restrict : 'A',
	    require : 'ngModel',
	    scope : {
	      showMeridian : '=',
	    },
	    link : function(scope, element, attrs, ngModel) {
	        var parseTime = function(viewValue) {

	        if (!viewValue) {
	          ngModel.$setValidity('time', true);
	          return null;
	        } else if (angular.isDate(viewValue) && !isNaN(viewValue)) {
	          ngModel.$setValidity('time', true);
	          return viewValue;
	        } else if (angular.isString(viewValue)) {
	          var timeRegex = /^(0?[0-9]|1[0-2]):[0-5][0-9] ?[a|p]m$/i;
	          if (!scope.showMeridian) {
	            timeRegex = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
	          }
	          if (!timeRegex.test(viewValue)) {
	            ngModel.$setValidity('time', false);
	            return undefined;
	          } else {
	            ngModel.$setValidity('time', true);
	            var date = new Date();
	            var sp = viewValue.split(":");
	            var apm = sp[1].match(/[a|p]m/i);
	            if (apm) {
	              sp[1] = sp[1].replace(/[a|p]m/i, '');
	              if (apm[0].toLowerCase() == 'pm') {
	                sp[0] = sp[0] + 12;
	              }
	            }
	            date.setHours(sp[0], sp[1]);
	            return date;
	          };
	        } else {
	          ngModel.$setValidity('time', false);
	          return undefined;
	        };
	      };

	      ngModel.$parsers.push(parseTime);

	      var showTime = function(data) {
	        parseTime(data);
	        var timeFormat = (!scope.showMeridian) ? "HH:mm" : "hh:mm a";
	        return $filter('date')(data, timeFormat);
	      };
	      ngModel.$formatters.push(showTime);
	      scope.$watch('showMeridian', function(value) {
	        var myTime = ngModel.$modelValue;
	        if (myTime) {
	          element.val(showTime(myTime));
	        }

	      });
	    }
	  };
	})	

	angularAMD.directive('myTimePicker', function($document, timepickerState) {
      return {
        restrict : 'AE',
        transclude : false,
        scope : {
          inputTime : "=",
          showMeridian : "=",
          disabled : "="
        },
        controller : function($scope, $element) {
          $scope.isOpen = false;
          
          $scope.disabledInt = angular.isUndefined($scope.disabled)? false : $scope.disabled;

          $scope.toggledate = function() {
            if ($scope.isOpen) {
              $scope.close();
            } else {
              $scope.open();
            }
          };

          $scope.toggletime = function() {
          	if ($scope.isOpen) {
          		$scope.close();
          	} else {
          		$scope.open();
          	}
          };

        	$scope.dateTimeNow = function() {
            // Return today's date and time
            var currentTime = new Date();

            // Return minutes
            var minute = currentTime.getMinutes();

            // Return hour
            var hour = currentTime.getHours();

            // returns the day of the month (from 1 to 31)
            var day = currentTime.getDate();

            // returns the month (from 0 to 11)
            var month = currentTime.getMonth() + 1;

            // returns the year (four digits)
            var year = currentTime.getFullYear()

            $scope.inputTime = new Date(year,month,day,hour,minute);            
        	};

        	$scope.dateTimeNow();       

        },
        link : function(scope, element, attrs) {
          var picker = {
        		  open : function () {
        			  timepickerState.closeAll();
        			  scope.isOpen = true;
        		  },
        		  close: function () {
        			  scope.isOpen = false;
        		  }
          		  
          }
          timepickerState.addPicker(picker);
          
          scope.open = picker.open;
          scope.close = picker.close;
          
          scope.$watch("disabled", function(value) {
            scope.disabledInt = angular.isUndefined(scope.disabled)? false : scope.disabled;
          });
          

          element.bind('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
          });

          $document.bind('click', function(event) {
            scope.$apply(function() {
           		scope.isOpen = false;
            });
          });

        },
        template : "<input type='text' class='form-control' ng-model='inputTime' name='time'  bs-timepicker data-time-format='HH:mm' data-length='1' "
            + "data-icon-up='icon-chevron-up' data-icon-down='icon-chevron-down' data-minute-step='1' data-arrow-behavior='picker'>"
            + "<span class='input-group-addon'><i class='icon-clock-o'></i></span>"
            + "<span class='input-group-addon'>Now</span>"
      };
  });

  angularAMD.directive('myDatePicker', function ($compile) {
      return {
          restrict: 'AE',
          scope : {
            inputDate : "=",
            dataMaxDate : "=",
            dataMinDate : "="
          },
          template: ""                  
          + "<input type='text' class='form-control' ng-model='inputDate' data-min-date='dataMinDate' data-max-date='dataMaxDate' data-autoclose='1' placeholder='Date' bs-datepicker>"
          + "<span class='input-group-addon'><i class='icon-calendar'></i></span>"          
          + "",          

          // controller: function ($scope) {
          //     $scope.$watch("dataMaxDate", function (bar) {
          //       console.log(dataMaxDate);
          //     });
          //     $scope.open = function ($event) {
          //         $event.preventDefault();
          //         $event.stopPropagation();
          //         $scope.openthis = {
          //             isOpened : true
          //         };
          //     };
          // }
          
      };
  });

  angularAMD.directive('myHeight', function ($window) {

    return {
        restrict: 'A',

        link: function (scope, elem, attrs) {

            var header = 46;
            var title = 70;
            var tabnav = 50;
            var subtitle = 40;     
            var actionbar = 35;
            var padmar = 80;
            var content = $window.innerHeight-header-title-tabnav-subtitle-actionbar-padmar;

            elem.css('height', content + 'px');
            // elem.css('overflow-y','scroll');
            // elem.css('overflow-x','hidden');
        }
    };

  });
  
  angularAMD.directive('dynamicheight', function ($window) {
    return function (scope, element, attr) {

        var w = angular.element($window);
        scope.$watch(function () {
            return {
                'h': window.innerHeight, 
                'w': window.innerWidth
            };
        }, function (newValue, oldValue) {
            console.log(newValue, oldValue);
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;

            scope.resizeWithOffset = function (offsetH) {
                scope.$eval(attr.notifier);
                return { 
                    'height': (newValue.h - offsetH) + 'px'                    
                };
            };

        }, true);

        w.bind('dynamicheight', function () {
            scope.$apply();
        });
    }
});
  
  angularAMD.directive('myDateTimePicker', function($document, timepickerState) {
      return {
        restrict : 'AE',
        transclude : false,
        scope : {
          inputTime : "=",
          showMeridian : "=",
          disabled : "="
        },
        controller : function($scope, $element) {
          $scope.isOpen = false;
          
          $scope.disabledInt = angular.isUndefined($scope.disabled)? false : $scope.disabled;

          $scope.toggledate = function() {
            if ($scope.isOpen) {
              $scope.close();
            } else {
              $scope.open();
            }
          };

          $scope.toggletime = function() {
            if ($scope.isOpen) {
              $scope.close();
            } else {
              $scope.open();
            }
          };

          $scope.dateTimeNow = function() {            
            // Return today's date and time
            var currentTime = new Date();

            // Return minutes
            var minute = currentTime.getMinutes();

            // Return hour
            var hour = currentTime.getHours();

            // returns the day of the month (from 1 to 31)
            var day = currentTime.getDate();

            // returns the month (from 0 to 11)
            var month = currentTime.getMonth() + 1;

            // returns the year (four digits)
            var year = currentTime.getFullYear()

            $scope.inputTime = new Date(year,month,day,hour,minute);            
          };

          $scope.dateTimeNow();       

        },
        link : function(scope, element, attrs) {
          var picker = {
              open : function () {
                timepickerState.closeAll();
                scope.isOpen = true;
              },
              close: function () {
                scope.isOpen = false;
              }
                
          }
          timepickerState.addPicker(picker);
          
          scope.open = picker.open;
          scope.close = picker.close;
          
          scope.$watch("disabled", function(value) {
            scope.disabledInt = angular.isUndefined(scope.disabled)? false : scope.disabled;
          });
          

          element.bind('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
          });

          $document.bind('click', function(event) {
            scope.$apply(function() {
              scope.isOpen = false;
            });
          });

        },
        template : ""                  
          + "<input type='text' class='form-control' ng-model='inputTime' data-min-date='dataMinDate' data-max-date='dataMaxDate' data-autoclose='1' placeholder='Date' bs-datepicker>"
          + "<span class='input-group-addon'><i class='icon-calendar'></i></span>"
          + "<input type='text' class='form-control' ng-model='inputTime' name='time'  bs-timepicker data-time-format='HH:mm' data-length='1' "
          + "data-icon-up='icon-chevron-up' data-icon-down='icon-chevron-down' data-minute-step='1' data-arrow-behavior='picker'>"
          + "<span class='input-group-addon'><i class='icon-clock-o'></i></span>"
          + "<span class='input-group-addon btn-now' ng-click='dateTimeNow()'>Now</span>"
          + ""
      };
  });

  angularAMD.directive('ngScrollbar', [
      '$timeout',
      '$parse',
      '$window',
      function ($timeout, $parse, $window) {
        return {
          restrict: 'A',
          replace: true,
          transclude: true,
          scope: { 'showYScrollbar': '=?isBarShown' },
          link: function (scope, element, attrs) {

            var mainElm, transculdedContainer, tools, thumb, thumbLine, track;
            var flags = { bottom: attrs.hasOwnProperty('bottom') };
            var win = angular.element($window);
            // Elements
            var dragger = { top: 0 }, page = { top: 0 };
            // Styles
            var scrollboxStyle, draggerStyle, draggerLineStyle, pageStyle;
            var calcStyles = function () {
              scrollboxStyle = {
                position: 'relative',
                overflow: 'hidden',
                'max-width': '100%',
                height: '100%'
              };
              if (page.height) {
                scrollboxStyle.height = page.height + 'px';
              }
              draggerStyle = {
                position: 'absolute',
                height: dragger.height + 'px',
                top: dragger.top + 'px'
              };
              draggerLineStyle = {
                position: 'relative',
                'line-height': dragger.height + 'px'
              };
              pageStyle = {
                position: 'relative',
                top: page.top + 'px',
                overflow: 'hidden'
              };
            };
            var redraw = function () {
              thumb.css('top', dragger.top + 'px');
              var draggerOffset = dragger.top / page.height;
              page.top = -Math.round(page.scrollHeight * draggerOffset);
              transculdedContainer.css('top', page.top + 'px');
            };
            var trackClick = function (event) {
              var offsetY = event.hasOwnProperty('offsetY') ? event.offsetY : event.layerY;
              var newTop = Math.max(0, Math.min(parseInt(dragger.trackHeight, 10) - parseInt(dragger.height, 10), offsetY));
              dragger.top = newTop;
              redraw();
              event.stopPropagation();
            };
            var wheelHandler = function (event) {
              var wheelDivider = 20;
              // so it can be changed easily
              var deltaY = event.wheelDeltaY !== undefined ? event.wheelDeltaY / wheelDivider : event.wheelDelta !== undefined ? event.wheelDelta / wheelDivider : -event.detail * (wheelDivider / 10);
              dragger.top = Math.max(0, Math.min(parseInt(page.height, 10) - parseInt(dragger.height, 10), parseInt(dragger.top, 10) - deltaY));
              redraw();
              if (!!event.preventDefault) {
                event.preventDefault();
              } else {
                return false;
              }
            };
            var lastOffsetY = 0;
            var thumbDrag = function (event, offsetX, offsetY) {
              dragger.top = Math.max(0, Math.min(parseInt(dragger.trackHeight, 10) - parseInt(dragger.height, 10), offsetY));
              event.stopPropagation();
            };
            var dragHandler = function (event) {
              var newOffsetX = 0;
              var newOffsetY = event.pageY - thumb[0].scrollTop - lastOffsetY;
              thumbDrag(event, newOffsetX, newOffsetY);
              redraw();
            };
            var _mouseUp = function (event) {
              win.off('mousemove', dragHandler);
              win.off('mouseup', _mouseUp);
              event.stopPropagation();
            };
            var _touchDragHandler = function (event) {
              var newOffsetX = 0;
              var newOffsetY = event.originalEvent.changedTouches[0].pageY - thumb[0].scrollTop - lastOffsetY;
              thumbDrag(event, newOffsetX, newOffsetY);
              redraw();
            };
            var _touchEnd = function (event) {
              win.off('touchmove', _touchDragHandler);
              win.off('touchend', _touchEnd);
              event.stopPropagation();
            };
            var buildScrollbar = function (rollToBottom) {
              // Getting top position of a parent element to place scroll correctly
              var parentOffsetTop = element[0].parentElement.offsetTop;
              var wheelEvent = win[0].onmousewheel !== undefined ? 'mousewheel' : 'DOMMouseScroll';
              rollToBottom = flags.bottom || rollToBottom;
              mainElm = angular.element(element.children()[0]);
              transculdedContainer = angular.element(mainElm.children()[0]);
              tools = angular.element(mainElm.children()[1]);
              thumb = angular.element(angular.element(tools.children()[0]).children()[0]);
              thumbLine = angular.element(thumb.children()[0]);
              track = angular.element(angular.element(tools.children()[0]).children()[1]);
              // Check if scroll bar is needed              
              page.height = element[0].offsetHeight - parentOffsetTop;
              if (page.height < 0) {
                page.height = element[0].offsetHeight;
              }
              page.scrollHeight = transculdedContainer[0].scrollHeight;
              if (page.height < page.scrollHeight) {
                scope.showYScrollbar = true;
                scope.$emit('scrollbar.show');
                // Calculate the dragger height
                dragger.height = Math.round(page.height / page.scrollHeight * page.height);
                dragger.trackHeight = page.height;
                // update the transcluded content style and clear the parent's
                calcStyles() 
                element.css({ overflow: 'hidden' });
                mainElm.css(scrollboxStyle);
                transculdedContainer.css(pageStyle);
                thumb.css(draggerStyle);
                thumbLine.css(draggerLineStyle);
                // Bind scroll bar events
                track.bind('click', trackClick);
                // Handl mousewheel
                transculdedContainer[0].addEventListener(wheelEvent, wheelHandler, false);
                // Drag the scroller with the mouse
                thumb.on('mousedown', function (event) {
                  lastOffsetY = event.pageY - thumb[0].offsetTop;
                  win.on('mouseup', _mouseUp);
                  win.on('mousemove', dragHandler);
                  event.preventDefault();
                });
                // Drag the scroller by touch
                thumb.on('touchstart', function (event) {
                  lastOffsetY = event.originalEvent.changedTouches[0].pageY - thumb[0].offsetTop;
                  win.on('touchend', _touchEnd);
                  win.on('touchmove', _touchDragHandler);
                  event.preventDefault();
                });
                if (rollToBottom) {
                  flags.bottom = false;
                  dragger.top = parseInt(page.height, 10) - parseInt(dragger.height, 10);
                } else {
                  dragger.top = Math.max(0, Math.min(parseInt(page.height, 10) - parseInt(dragger.height, 10), parseInt(dragger.top, 10)));
                }
                redraw();
              } else {
                scope.showYScrollbar = false;
                scope.$emit('scrollbar.hide');
                thumb.off('mousedown');
                transculdedContainer[0].removeEventListener(wheelEvent, wheelHandler, false);
                transculdedContainer.attr('style', 'position:relative;top:0');
                // little hack to remove other inline styles
                mainElm.css({ height: '100%' });
              }
            };
            var rebuildTimer;
            var rebuild = function (e, data) {
              /* jshint -W116 */
              if (rebuildTimer != null) {
                clearTimeout(rebuildTimer);
              }
              /* jshint +W116 */
              var rollToBottom = !!data && !!data.rollToBottom;
              rebuildTimer = setTimeout(function () {
                page.height = null;
                buildScrollbar(rollToBottom);
                if (!scope.$$phase) {
                  scope.$digest();
                }
                // update parent for flag update
                if (!scope.$parent.$$phase) {
                  scope.$parent.$digest();
                }
              }, 72);
            };
            buildScrollbar();
            if (!!attrs.rebuildOn) {
              attrs.rebuildOn.split(' ').forEach(function (eventName) {
                scope.$on(eventName, rebuild);
              });
            }
            if (attrs.hasOwnProperty('rebuildOnResize')) {
              win.on('resize', rebuild);
            }
          },
          template: '<div>' + '<div class="ngsb-wrap">' + '<div class="ngsb-container" ng-transclude></div>' + '<div class="ngsb-scrollbar" style="position: absolute; display: block;" ng-show="showYScrollbar">' + '<div class="ngsb-thumb-container">' + '<div class="ngsb-thumb-pos" oncontextmenu="return false;">' + '<div class="ngsb-thumb" ></div>' + '</div>' + '<div class="ngsb-track"></div>' + '</div>' + '</div>' + '</div>' + '</div>'
        };
      }
  ]);


});