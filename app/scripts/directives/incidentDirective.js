define(['angularAMD'], function (angularAMD) {
	
  angularAMD.directive('incidentFilterBar', function ($compile) {
      return {
          restrict: 'AE',
           template: '<div>'
				    + '<div class="form-seperator-top"></div>'
				      + '<div class="row form-inline">'
				        + '<div class="col-xs-12 ctr-filter">'
				             + '<button container="body" class="btn btn-filter" ng-click="tablefilter = !tablefilter" ng-init="tablefilter = true">Filter<i class="marR10 icon-chevron-down icon-blue "></i>'
				              + '</button>'
				            + '<button class="btn btn-filter" bs-dropdown="dropdown" data-template="dropdown/custom-dropdown.tpl.html" data-html="true">'
				              + 'More Action<i class="marR10 icon-chevron-down icon-blue "></i>'
				          + '</div>'
				        + '</div>'
				      + '</div>'
				    + '<div class="form-seperator-bottom"></div>'
				  + '</div>'
          
      };
  });

});