require.config({
  baseUrl: 'scripts/',

  // alias libraries paths.  Must set 'angular'
  paths: {
    'angular': 'ext/angular',
    'angular-route': 'ext/angular-route',
    'angular-ui-router': 'ext/angular-ui-router',
    'angular-sanitize': 'ext/angular-sanitize',
    'angularAMD': 'ext/angularAMD',
    'ngload': 'ext/ngload',
    'angular-resource': 'ext/angular-resource',
    'jquery':'ext/jquery',
    'jquery-ui':'ext/jquery-ui',
    // 'strap':'ext/angular-strap',
    'strap':'custom/angular-strap-custom',
    'strap-tpls':'ext/angular-strap.tpl',
    'ng-grid':'ext/ng-grid',
    'template-load':'template/template',
    'ui-extras':'ext/ct-ui-router-extras',
    'angular-animate':'ext/angular-animate',
    'anuglar-loading':'ext/loading-bar'
  },

  // Add angular modules that does not support AMD out of the box, put it in a shim
  shim: {
    'angular-route': [ 'angular' ],
    'angularAMD': [ 'angular' ],
    'ngload': [ 'angularAMD' ],
    'angular-resource': [ 'angular' ],
    'angular-ui-router': [ 'angular' ],
    'angular-sanitize': [ 'angular' ],
    'jquery': [ 'angular' ],
    'jquery-ui': [ 'angular' ],
    // 'strap': [ 'angular' ],
    'strap': [ 'angular' ],
    'strap-tpls': [ 'strap' ],
    'template-load': [ 'angular' ],
    'ng-grid': [ 'jquery' ],
    'ui-extras':[ 'angular-ui-router'],
    'angular-animate':[ 'anuglar-loading'],
    'anuglar-loading':[ 'angular' ]
  },

  // kick start application
  deps: ['app']
});
