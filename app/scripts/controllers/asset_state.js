define(['angularAMD'], function (angularAMD) {
	'use strict';
	angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('cis', angularAMD.route({
			parent : 'asset',
			url: '/cis',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/asset-cis.html',
					controllerUrl: 'controllers/asset_cis_ctrl'
				})
			}
		}))
		;

    // Else -- This is not working for some reason:
    $urlRouterProvider
    .when('/dashboard', '/dashboard/asset');

}]);

});