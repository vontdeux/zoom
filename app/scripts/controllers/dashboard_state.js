define(['angularAMD','controllers/incident_state','controllers/change_state','controllers/problem_state',
	'controllers/service_state','controllers/supplier_state','controllers/asset_state'], function (angularAMD) {
		'use strict';

		angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

			$stateProvider
			.state('list', angularAMD.route({
				parent : 'dashboard',
				url: '/list',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/dashboard/list.html',
						controllerUrl: 'controllers/list'
					})
				}
			}))
			.state('incident', angularAMD.route({
				parent : 'dashboard',
				url: '/incident',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/layout/incident.html',
						controllerUrl: 'controllers/incident_ctrl'
					})
				}
			}))
			.state('rdown', angularAMD.route({
				parent : 'incident',
				url: '/rdown',
				views: {
					'submenuContent' : angularAMD.route({
						templateUrl: 'views/layout/incident-rtop.html',
						controllerUrl: 'controllers/incident_rtop_ctrl',
					}),
					'submenuContent2' : angularAMD.route({
						templateUrl: 'views/layout/incident-rtop-detail.html',
						controllerUrl: 'controllers/incident_rtop_ctrl'
					})
				}
			}))		
			.state('change', angularAMD.route({
				parent : 'dashboard',
				url: '/change',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/layout/change.html',
						controllerUrl: 'controllers/change_ctrl'
					})
				}
			}))
			.state('problem', angularAMD.route({
				parent : 'dashboard',
				url: '/problem',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/layout/problem.html',
						controllerUrl: 'controllers/problem_ctrl'
					})
				}
			}))
			.state('supplier', angularAMD.route({
				parent : 'dashboard',
				url: '/supplier',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/layout/supplier.html',
						controllerUrl: 'controllers/supplier_ctrl'
					})
				}
			}))
			.state('service', angularAMD.route({
				parent : 'dashboard',
				url: '/service',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/layout/service.html',
						controllerUrl: 'controllers/service_ctrl'
					})
				}
			}))
			.state('asset', angularAMD.route({
				parent : 'dashboard',
				url: '/asset',
				views: {
					'subContent' : angularAMD.route({
						templateUrl: 'views/layout/asset.html',
						controllerUrl: 'controllers/asset_ctrl'
					})
				}
			}))
			;

// Else -- This is not working for some reason:
$urlRouterProvider
.when('/dashboard', '/');

}]);

});