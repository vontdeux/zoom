define(['angularAMD'], function (angularAMD) {
	'use strict';
	angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('scorecard', angularAMD.route({
			parent : 'supplier',
			url: '/scorecard',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/supplier-scorecard.html',
					controllerUrl: 'controllers/supplier_scorecard_ctrl'
				})
			}
		}))
		.state('supplierTicket', angularAMD.route({
			parent : 'supplier',
			url: '/supplier/ticket',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/supplier-ticket.html',
					controllerUrl: 'controllers/supplier_ticket_ctrl'
				})
			}
		}))
		;

    // Else -- This is not working for some reason:
    $urlRouterProvider
    .when('/dashboard', '/dashboard/supplier');

}]);

});