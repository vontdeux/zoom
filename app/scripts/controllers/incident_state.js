define(['angularAMD'], function (angularAMD) {
	'use strict';
	angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('rtop', angularAMD.route({
			parent : 'incident',
			url: '/rtop',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/incident-rtop.html',
					controllerUrl: 'controllers/incident_rtop_ctrl'
				})
			}
		}))
		.state('rtopDefault', angularAMD.route({
			parent : 'rtop',
			url: '/rtop/detail/default/:rtopID',
			views: {
				'contentDefault': angularAMD.route({
					templateUrl: 'views/layout/incident-rtop-detail.html',
					controllerUrl: 'controllers/incident_rtop_detail_ctrl'
				})
			}
		}))
		.state('rtopHorizontal', angularAMD.route({
			parent : 'rtop',
			url: '/rtop/detail/horizontal/:rtopID',
			views: {
				'contentHorizontal': angularAMD.route({
					templateUrl: 'views/layout/incident-rtop-detail.html',
					controllerUrl: 'controllers/incident_rtop_detail_ctrl'
				})
			}
		}))
		.state('rtopVertical', angularAMD.route({
			parent : 'rtop',
			url: '/rtop/detail/vertical/:rtopID',
			views: {
				'contentVertical': angularAMD.route({
					templateUrl: 'views/layout/incident-rtop-detail.html',
					controllerUrl: 'controllers/incident_rtop_detail_ctrl'
				})
			}
		}))
		.state('createIMCL', angularAMD.route({
			parent : 'incident',
			url: '/createIMCL',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/incident-createimcl.html',
					controllerUrl: 'controllers/incident_createimcl_ctrl'
				})
			}
		}))
		.state('listIMIS', angularAMD.route({
			parent : 'incident',
			url: '/IMIS',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/incident-imis.html',
					controllerUrl: 'controllers/incident_imis_ctrl'
				})
			}
		}))
		.state('createIMIS', angularAMD.route({
			parent : 'incident',
			url: '/createIMIS',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/incident-createimis.html',
					controllerUrl: 'controllers/incident_createimis_ctrl'
				})
			}
		}))
		.state('listRFO', angularAMD.route({
			parent : 'incident',
			url: '/RFO',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/incident-rfo.html',
					controllerUrl: 'controllers/incident_rfo_ctrl'
				})
			}
		}))
		.state('createRFO', angularAMD.route({
			parent : 'incident',
			url: '/createRFO',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/incident-createrfo.html',
					controllerUrl: 'controllers/incident_createrfo_ctrl'
				})
			}
		}))
		;

    // Else -- This is not working for some reason:
    $urlRouterProvider
    .when('/dashboard', '/dashboard/incident');

}]);

});