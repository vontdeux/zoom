define(['angularAMD'], function (angularAMD) {
	'use strict';
	angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('hpessx', angularAMD.route({
			parent : 'service',
			url: '/hpessx',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-hpessx.html',
					controllerUrl: 'controllers/service_hpessx_ctrl'
				})
			}
		}))
		.state('sxnetwork', angularAMD.route({
			parent : 'service',
			url: '/sxnetwork',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-sx-network.html',
					controllerUrl: 'controllers/service_sx_network_ctrl'
				})
			}
		}))
		.state('sxalerts', angularAMD.route({
			parent : 'service',
			url: '/sxalerts',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-sx-alerts.html',
					controllerUrl: 'controllers/service_sx_alerts_ctrl'
				})
			}
		}))
		.state('sxgo2green', angularAMD.route({
			parent : 'service',
			url: '/sxgo2green',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-sx-go-2-green.html',
					controllerUrl: 'controllers/service_sx_go_2_green_ctrl'
				})
			}
		}))
		.state('sxagenda', angularAMD.route({
			parent : 'service',
			url: '/sxagenda',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-sx-agenda.html',
					controllerUrl: 'controllers/service_sx_agenda_ctrl'
				})
			}
		}))
		.state('sxalertscreate', angularAMD.route({
			parent : 'service',
			url: '/sxalerts-create',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-sx-create.html',
					controllerUrl: 'controllers/service_sx_create_ctrl'
				})
			}
		}))
		.state('sxagendaupdate', angularAMD.route({
			parent : 'service',
			url: '/sxagenda-update',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/service-sx-update.html',
					controllerUrl: 'controllers/service_sx_update_ctrl'
				})
			}
		}))
		.state('sxquestionNew', angularAMD.route({
			parent : 'service',
			url: '/sxquestion-new',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/partial/new-question.html'
				})
			}
		}))
		.state('sxquestionOngoing', angularAMD.route({
			parent : 'service',
			url: '/sxquestion-ongoing',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/partial/ongoing-question.html'
				})
			}
		}))
		.state('sxquestionMonitoring', angularAMD.route({
			parent : 'service',
			url: '/sxquestion-monitoring',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/partial/monitoring-question.html'
				})
			}
		}))
		.state('sxquestionCompleted', angularAMD.route({
			parent : 'service',
			url: '/sxquestion-completed',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/partial/completed-question.html'
				})
			}
		}))
		;

    // Else -- This is not working for some reason:
    $urlRouterProvider
    .when('/dashboard', '/dashboard/service');

}]);

});