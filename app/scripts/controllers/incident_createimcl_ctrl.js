define(['app','directives/timepicker','services/imclServices','custom/ngGridFlexibleHeightPlugin'], function (app) {
    'use strict';
    return ['$scope','$resource','$http','$filter','$timeout', 'IMCLJsonService' , 
    function ($scope,$resource,$http,$filter,$timeout,IMCLJsonService) {
        
        $scope.imcl = IMCLJsonService.getimcl.getData();
        
        $scope.selected = "";        

        $scope.config1 = [];
        $scope.config2 = [];
        $scope.check1 = [];
        $scope.timeline = [];
        $scope.keycontacts = [];
        $scope.actionplans = [];

        // dummy selection
        $scope.selectedIcons = [];
        $scope.icons = [
            {value: 'All', label: 'All'},
            {value: 'None', label: 'None'},
            {value: '7-Eleven', label: '7-Eleven'},
            {value: 'AAA', label: 'AAA'},
            {value: 'AAMVA', label: 'AAMVA'},
            {value: 'AAPTLIMITED', label: 'AAPTLIMITED'},
            {value: 'aBBRY', label: 'aBBRY'},
            {value: 'ADEECCO', label: 'AAbA'},
            {value: 'ADIDAS', label: 'AAMVbA'},
            {value: 'AGROSTAR', label: 'AAPT LIMITED'},
            {value: 'AIRGAS', label: 'AIRGAS'},
            {value: 'AHILD', label: 'AHILD'},
            {value: 'ALOHA', label: 'ALOHA'},
            {value: 'AMERUS', label: 'AMERUS'},
            {value: 'ANDHRA', label: 'ANDHRA'},
            {value: 'AAaA', label: 'AAaA'},
            {value: 'AAMVaA', label: 'AAMVaA'},
            {value: 'AMLIM', label: 'AMLIM'}
        ];

        // $scope.config1 = $scope.selected.config;
        var opts =  { minHeight: 0 , maxHeight: 500 };
        $scope.config1columnDefs = [
            { field: 'devicename', displayName: 'Device Name'},
            { field: 'dedicated', displayName: 'Leverage'},
            { field: 'devicetype', displayName: 'Device Type'},
            { field: 'nocregion', displayName: 'NOC Region'},            
            { field: 'model', displayName: 'Rtop Start Date'},
            { field: 'nocresponsible', displayName: 'NOC Responsible'},
            { field: 'vendor', displayName: 'Vendor'},
            { field: 'responsibleparty', displayName: 'Responsible Party'},
            { field: 'location', displayName: 'Location'},
            { field: 'note', displayName: 'Note'}
            ]; 
        $scope.config2columnDefs = [
            { field: 'replacement', displayName: 'Serial #'},
            { field: 'rmapartvalidated', displayName: 'Time RMA Part Validated'},
            { field: 'rmanote', displayName: 'Note'}
            ];
        $scope.check1columnDefs = [
            { field: 'quickrestoretime', displayName: 'Quick Restore Taken'},
            { field: 'remote', displayName: 'Remote'},
            { field: 'remotemethod', displayName: 'Method'},
            { field: 'ciname', displayName: 'CI Name'},
            { field: 'success', displayName: 'Success'},
            { field: 'resilience', displayName: 'Did Resilience/Redundancy Work'},
            { field: 'note', displayName: 'Note'}
            ];
        $scope.timelinecolumnDefs = [
            { field: 'timeline', displayName: 'Time'},
            { field: 'notification', displayName: 'Add to Notification?'},
            { field: 'activity', displayName: 'Incident Management Activity'}
            ];
        $scope.keycontactscolumnDefs = [
            { field: 'otherpartyinvolvetime', displayName: 'Time'},
            { field: 'name', displayName: 'Name'},
            { field: 'team', displayName: 'Team'}
            ];
        $scope.actionplanscolumnDefs = [
            { field: 'description', displayName: 'Description'},
            { field: 'expectation', displayName: 'Expectation'},
            { field: 'actionstatus', displayName: 'Status'},
            { field: 'targetdatetime', displayName: 'Target Date'},
            { field: 'name', displayName: 'Action Owner'},
            { field: 'completiondatetime', displayName: 'Completion Date'}
            ];

        // to resize ng-grid
        $scope.$watch('miniMenu', function (newVal, oldVal) {
          if (newVal !== oldVal) {
            $(window).resize();
            }
        }, true);

        $scope.grid1 = { 
            data: 'config1',
            columnDefs: 'config1columnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]            
        };

        $scope.grid2 = { 
            data: 'config2',
            columnDefs: 'config2columnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]
        };

        $scope.grid3 = { 
            data: 'check1',
            columnDefs: 'check1columnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]
        };

        $scope.grid4 = { 
            data: 'timeline',
            columnDefs: 'timelinecolumnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]
        };

        $scope.grid5 = { 
            data: 'keycontacts',
            columnDefs: 'keycontactscolumnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]
        };

        $scope.grid6 = { 
            data: 'actionplans',
            columnDefs: 'actionplanscolumnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]
        };

        $scope.addconfig1 = function(){              
            var obj = angular.copy($scope.selected.config1);                
            $scope.config1.push(obj);            
        };

        $scope.addconfig2 = function(){
            var obj = angular.copy($scope.selected.config2);
            $scope.config2.push(obj);  
        };

        $scope.addcheck1 = function(){
           var obj = angular.copy($scope.selected.check1);
            $scope.check1.push(obj);  
        };

        $scope.addtimeline = function(){
            var obj = angular.copy($scope.selected.timeline);
            $scope.timeline.push(obj);  
        };

        $scope.addkeycontact = function(){
           var obj = angular.copy($scope.selected.keycontacts);
            $scope.keycontacts.push(obj);  
        };

        $scope.addactionplan = function(){
           var obj = angular.copy($scope.selected.actionplans);
            $scope.actionplans.push(obj);  
        };
        
        $scope.selectpanel = function(i){
            $scope.panels.activePanels = [i];
            $scope.imcl.panels[i].isOpen = true;
        };        

        $scope.expandall = function(){
            var obj = [];
            var i;            
            
            for(i=0; i < $scope.imcl.panels.length; i++){
                obj.push(i);    
            }                
                        
            $scope.panels.activePanels = obj;
            for(i=0; i < $scope.imcl.panels.length; i++){
                $scope.imcl.panels[i].isOpen = true;
            };
        };

        $scope.collapseall = function(){
            var i;
            $scope.panels.activePanels = [];
            for(i=0; i < $scope.imcl.panels.length; i++){
                $scope.imcl.panels[i].isOpen = false;
            };
        };
        

        $scope.scrolltop = function(){
            $("body").animate({scrollTop: 0}, "fast");
        };
    

        // Tabs directive (initial, update, final)
        $scope.tabs = [
            {title:'Initial', page: 'views/partial/imcl-initial.html'},
            {title:'Update', page: 'views/partial/imcl-update.html'},
            {title:'Final', page: 'views/partial/imcl-final.html'}
        ];
        $scope.tabs.activeTab = 0;
        $scope.$broadcast('rebuild:me');
        // Tabs list for panels
        // $scope.panels = {activePanels: []};        
        
        $scope.exports = [
        {
            "text": '<i class="marR10 icon-envelope-o icon-blue"></i>Email',
            "href": "#"
        },
        {
            "text": '<i class="marR10 icon-print icon-blue"></i>Print',
            "click": "#"
        },
        {
        "divider": true
        },
        {
            "text": '<i class="marR10 icon-file-pdf-o icon-blue"></i>Pdf',
            "click": "#"
        },
        {
            "text": '<i class="marR10 icon-file-excel-o icon-blue"></i>Excel',
            "click": "#"
        },
        {
            "text": '<i class="marR10 icon-file-word-o icon-blue"></i>Word',
            "click": "#"
        },
        {
        "divider": true
        },
        {
            "text": '<i class="marR10 icon-star icon-blue"></i>Favourite',
            "click": "#"
        }
        ];


        

    }];
});
