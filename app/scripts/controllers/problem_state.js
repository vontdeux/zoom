define(['angularAMD'], function (angularAMD) {
	'use strict';
	angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('rtoprca', angularAMD.route({
			parent : 'problem',
			url: '/rtoprca',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/problem-rtop-rca.html',
					controllerUrl: 'controllers/problem_rtop_rca_ctrl'
				})
			}
		}))
		;

    // Else -- This is not working for some reason:
    $urlRouterProvider
    .when('/dashboard', '/dashboard/problem');

}]);

});