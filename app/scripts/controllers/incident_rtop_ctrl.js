define(['app','services/restService','directives/incidentDirective','custom/ngGridFlexibleHeightPlugin'], function (app) {
    'use strict';
    return ['$scope','$resource','rtopList','$http','$filter','$timeout','$state','$modal','incidentMasterData',
    function ($scope,$resource,rtopList,$http,$filter,$timeout,$state,$modal,incidentMasterData) {

    $scope.selectedResponsibleOwner = [];
    $scope.selectedRtopCapability = [];
    $scope.selectedRtopClient = [];
    $scope.selectedRtopRegion = [];
    $scope.selectedRtopSeverity = [];
    $scope.selectedRtopStatus = [];
    $scope.selectedRtopType = [];

    incidentMasterData.query(function(data){
        $scope.responsibleOwner = data[0].responsibleOwner;
        $scope.rtopCapability = data[0].rtopCapability;
        $scope.rtopClient = data[0].client;
        $scope.rtopRegion = data[0].region;
        $scope.rtopSeverity = data[0].rtopSeverity;
        $scope.rtopStatus = data[0].rtopStatus;
        $scope.rtopType = data[0].rtopType;
    });
       


    
    // dummy selection
    // $scope.selectedIcons = [];
    //     $scope.icons = [
    //     {value: 'All', label: 'All'},
    //     {value: 'None', label: 'None'},
    //     {value: '7-Eleven', label: '7-Eleven'},
    //     {value: 'AAA', label: 'AAA'},
    //     {value: 'AAMVA', label: 'AAMVA'},
    //     {value: 'AAPTLIMITED', label: 'AAPTLIMITED'},
    //     {value: 'aBBRY', label: 'aBBRY'},
    //     {value: 'ADEECCO', label: 'AAbA'},
    //     {value: 'ADIDAS', label: 'AAMVbA'},
    //     {value: 'AGROSTAR', label: 'AAPT LIMITED'},
    //     {value: 'AIRGAS', label: 'AIRGAS'},
    //     {value: 'AHILD', label: 'AHILD'},
    //     {value: 'ALOHA', label: 'ALOHA'},
    //     {value: 'AMERUS', label: 'AMERUS'},
    //     {value: 'ANDHRA', label: 'ANDHRA'},
    //     {value: 'AAaA', label: 'AAaA'},
    //     {value: 'AAMVaA', label: 'AAMVaA'},
    //     {value: 'AMLIM', label: 'AMLIM'}
    // ];

    // rtopSelectbox.query(function(data){
    //     console.log(data[0]);
    //     $scope.responsibleOwner = data[0].responsibleOwner
    //     $scope.rtopCapability = data[0].rtopCapability
    //     $scope.rtopClient = data[0].rtopClient
    //     $scope.rtopRegion = data[0].rtopRegion
    //     $scope.rtopSeverity = data[0].rtopSeverity
    //     $scope.rtopStatus = data[0].rtopStatus
    //     $scope.rtopType = data[0].rtopType
    // })

    // action | export dropdown
    $scope.exports = [
        {
            "text": '<i class="marR10 icon-envelope-o icon-blue"></i>Email',
            "href": "#"
        },
        {
            "text": '<i class="marR10 icon-print icon-blue"></i>Print',
            "click": "#"
        },
        {
        "divider": true
        },
        {
            "text": '<i class="marR10 icon-file-pdf-o icon-blue"></i>Pdf',
            "click": "#"
        },
        {
            "text": '<i class="marR10 icon-file-excel-o icon-blue"></i>Excel',
            "click": "#"
        },
        {
            "text": '<i class="marR10 icon-file-word-o icon-blue"></i>Word',
            "click": "#"
        },
        {
        "divider": true
        },
        {
            "text": '<i class="marR10 icon-star icon-blue"></i>Favourite',
            "click": "#"
        }
    ];

    //dynamic layour
    $scope.showNothing = true;
    $scope.showOriginal = true;
    $scope.showDefault = true;
    $scope.showHorizontal = true;
    $scope.showHorizontalCss = false;
    $scope.showVertical = true;
    $scope.userLayout = false;

    //date scope
    $scope.fromDate = new Date();
    $scope.untilDate = new Date();

    //scope
    $scope.title = 'RtOP';
    $scope.selectedrtopColumn = '';
    $scope.selectedrtopColumns = [];
    $scope.rtopColumns = [];
    $scope.fromDate = '';
    $scope.untilDate = '';
    $scope.useClient = {};

    $scope.dateOptions = {formatYear: 'yy',startingDay: 1};
    $scope.formats = ['MM-dd-yyyy','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.updateFilter = function (item){
        console.log(item);
        $scope.filterOptions.filterText = '';
    }

    //email form launcher
    $scope.openItem = function(value){
        if(value === 'email'){
            $scope.tableEmail = true;
        }
        if($scope.tablefilter === true){
            $scope.tablefilter = false;
        }
        // var myModal = $modal({scope:$scope,template:'template/modal/email.html', show: true});
    }

    $scope.closeItem = function(value){
        if(value === 'email'){
            $scope.tableEmail = false;
        }
    }

    // function for openup horizontal|vertical|default
    $scope.openSide = function (nothing,stat,layout,id) {
        $scope.rowID = id;
        $scope.showNothing = nothing;
        if(stat === 'view'){
            $scope.view = stat;
            $scope.edit = false;
        }else{
            $scope.edit = stat;
            $scope.view = false;
        }            
        console.log(nothing);
        console.log(layout);
        if(nothing === true){
            console.log("HAHA");
            $scope.showOriginal = true;
            $scope.showDefault = false;
            $scope.showHorizontal = false;
            $scope.showVertical = false;
            $scope.showHorizontalCss = false;
            $state.go('rtop');
        }else{
            if(layout === 'default'){
                console.log("HAHA1");
                $scope.showOriginal = false;
                $scope.showDefault = true;
                $scope.showHorizontal = false;
                $scope.showVertical = false;
                $state.go('rtopDefault', {'rtopID': id });
                // $scope.showOriginal = true;
                // $scope.showDefault = false;
                // $scope.showHorizontal = true;
                // $scope.showVertical = false;
                // $scope.showHorizontalCss = true;
                // $state.go('rtopHorizontal', {'rtopID': id });
            }else if(layout === 'horizontal'){
                console.log("HAHA2");
                $scope.showOriginal = true;
                $scope.showDefault = false;
                $scope.showHorizontal = true;
                $scope.showVertical = false;
                $scope.showHorizontalCss = true;
                $state.go('rtopHorizontal', {'rtopID': id });
            }else if(layout === 'vertical'){
                console.log("HAHA3");
                $scope.showOriginal = true;
                $scope.showDefault = false;
                $scope.showHorizontal = false;
                $scope.showVertical = true;
                $scope.showHorizontalCss = false;
                $state.go('rtopVertical', {'rtopID': id });
            }
        }
        $(window).resize(); 
    }

    //function for search button
    $scope.getNewRtopData = function (val,val2){
        console.log($scope.gridOptions.$gridScope)

        console.log(val)
        console.log(val2)
        // $scope.showcontent = true;
        var date1 = $filter('date')(val, "MM-dd-yyyy");
        var date2 = $filter('date')(val2, "MM-dd-yyyy");

        console.log(date1)
        console.log(date2)

        rtopList.query({startdt:date1, enddt:date2, rtopno:'0', tooluserid:'90', toolmoduleid:'1'}, function(data){
            console.log(data[0].rtopSummary)
            $scope.filteredData = data[0].rtopSummary;
        });
    }

    //date function - need to check
    $scope.today = function() {
        $scope.dt = new Date();
        $scope.startdate = new Date();
        $scope.enddate = new Date();
    };
    $scope.today();
    
    //date function - need to check
    $scope.clear = function () {
        $scope.dt = null;
        $scope.startdate = null;
        $scope.enddate = null;
    };

    //date function - need to check // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    //date function - need to check
    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    //function need to check
    $scope.open = function($event,opened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope[opened] = true;
    };

    // $scope.filterNephi = function() {
    //     var filterText = 'name:Nephi';
    //     if ($scope.filterOptions.filterText === '') {
    //       $scope.filterOptions.filterText = filterText;
    //     }
    //     else if ($scope.filterOptions.filterText === filterText) {
    //       $scope.filterOptions.filterText = '';
    //     }
    // };

    
    //ng-grid
    

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };

    $scope.totalServerItems = 0;

    $scope.pagingOptions = {
        pageSizes: [50, 60, 80],
        pageSize: 30,
        currentPage: 1    
    };

    $scope.numPages = function () {
        return Math.ceil(data.length / $scope.numPerPage);
    };

    $scope.setPagingData = function(data,page,pageSize){
        //client side paging need to remove for server paging
        var totalpage = Math.ceil(data.length / pageSize);
        if(page > totalpage){
            $scope.pagingOptions.currentPage = 1
            page = 1;
        }

        //client side paging
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);

        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;

        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
        var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                data = $scope.filteredData.filter(function(item) {
                return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                });
            $scope.setPagingData(data,page,pageSize);
            } else {
                var largeLoad = $scope.filteredData;
                largeLoad.length
                $scope.setPagingData(largeLoad,page,pageSize);
            }
        },0);
    };

    // view by list
    $scope.getlist = function () {
        for(var i=0; i < $scope.gridOptions.$gridScope.columns.length ; i++ ){
            var data = {};
            var data2 = {};
            data.value = $scope.gridOptions.$gridScope.columns[i].index;
            data.label = $scope.gridOptions.$gridScope.columns[i].displayName;
            // console.log($scope.gridOptions.$gridScope.columns)
            $scope.rtopColumns.push(data);
            if(true === $scope.gridOptions.$gridScope.columns[i].visible){
            $scope.selectedrtopColumns.push($scope.gridOptions.$gridScope.columns[i].index)   
            }
        }
    }

    //show | hide column
    $scope.openMe = function (i){
        $scope.gridOptions.$gridScope.columns[i].toggleVisible()
    }


    $scope.celltemplate = 
    '<div data-auto-close=\"1\" data-template=\"template/popover/incident-grid.html\" data-placement=\"right-bottom\" title=\"{{popover.title}}\"'+
    'data-container=\"body\" data-html=\"1\" data-title=\"{{title}}\" data-content=\"{{row.entity}}\" data-trigger=\"hover\" bs-popover class=\"ngCellText\"'+
    'ng-class=\"col.colIndex()\"><a ng-click=\"openSide(\'false\',\'view\',selectedLayout,row.entity.RTOP_ID)\"><span ng-cell-text>{{row.getProperty(col.field)}}</span>'+
    '</a><span style="padding-left:5px;"><i ng-click=\"openSide(\'false\',\'edit\',selectedLayout,row.entity.RTOP_ID)\" class=\"btn-circle icon-pencil2\"></i></span></div>'


    $scope.rowtemplate = 
    '<div ng-style="{\'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell></div>'


    $scope.footerTemplate = 
    '<div ng-show=\"showFooter\" class=\"ngFooterPanel\" ng-class=\"{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}\"'+
    'ng-style=\"footerStyle()\"><div class=\"ngTotalSelectContainer\"><div class=\"ngFooterTotalItems\" ng-class=\"{\'ngNoMultiSelect\': !multiSelect}\" >'+
    '<span class=\"ngLabel\">{{i18n.ngTotalItemsLabel}} {{maxRows()}}</span>'+
    '<span ng-show=\"filterText.length > 0\" class=\"ngLabel\">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span>'+
    '</div><div class=\"ngFooterSelectedItems\" ng-show=\"multiSelect\"><span class=\"ngLabel\">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span>'+
    '</div></div><div class=\"form-group ngPagerContainer\" style=\"float: right; margin-top: 10px;\" ng-show=\"enablePaging\" '+
    'ng-class=\"{\'ngNoMultiSelect\': !multiSelect}\"><div style=\"float: left; margin-right: 25px; margin-top: 4px;;\" class=\"ngRowCountPicker\">'+
    '<span style=\"float: left; margin-top: 3px; margin-right:10px;\" class=\"ngLabel\">{{i18n.ngPageSizeLabel}}</span>'+
    '<input type=\"text\" class=\"form-control\" style=\"float: left;height: 27px; width: 60px\" ng-model=\"pagingOptions.pageSize\">  '+
    '</div><div style=\"float:left; margin-right: 30px; line-height:25px;\" class=\"form-group ngPagerControl\" style=\"float: left; min-width: 135px;\">'+
    '<button type=\"button\" class=\" btn btn-hp ngPagerButton\" ng-click=\"pageToFirst()\" ng-disabled=\"cantPageBackward()\" '+
    'title=\"{{i18n.ngPagerFirstTitle}}\"><div class=\"ngPagerFirstTriangle\"><div class=\"ngPagerFirstBar\"></div></div></button>'+
    '<button type=\"button\" class=\" btn  btn-hp   ngPagerButton\" ng-click=\"pageBackward()\" ng-disabled=\"cantPageBackward()\" '+
    'title=\"{{i18n.ngPagerPrevTitle}}\"><div class=\"ngPagerFirstTriangle ngPagerPrevTriangle\"></div></button>'+
    '<input class=\"btn btn-default ngPagerCurrent\" min=\"1\" max=\"{{currentMaxPages}}\" type=\"number\" '+
    'style=\"width: 50px;height: 25px;margin-top: 5px;padding: 0 4px;\" ng-model=\"pagingOptions.currentPage\"/>'+
    '<span class=\"btn ngGridMaxPagesNumber\" ng-show=\"maxPages() > 0\">/ {{maxPages()}}</span>'+
    '<button type=\"button\" class=\" btn  btn-hp  ngPagerButton\" ng-click=\"pageForward()\" ng-disabled=\"cantPageForward()\" '+
    'title=\"{{i18n.ngPagerNextTitle}}\"><div class=\"ngPagerLastTriangle ngPagerNextTriangle\"></div></button>'+
    '<button type=\"button\" class=\" btn  btn-hp  ngPagerButton\" ng-click=\"pageToLast()\" ng-disabled=\"cantPageToLast()\" '+
    'title=\"{{i18n.ngPagerLastTitle}}\"><div class=\"ngPagerLastTriangle\"><div class=\"ngPagerLastBar\"></div></div></button>'+
    '</div></div></div>'

    $scope.celltemplateClient = 
    '<div class=\"ngCellText\" ng-class=\"col.colIndex()\">'+
    '<span style=\"padding-right:10px;\" ng-cell-text ng-repeat=\"item in row.entity.rtopAffectedClients\">{{item.clientDesc}}</br></span></div>'

    $scope.columnDefs = [{ field: 'rtopID', displayName: 'RtOP #', cellTemplate:$scope.celltemplate, cellClass: 'cellPopover'},
    { field: 'rtopStartDt', displayName: 'RtOP Start Date'},
    { field: 'rtopEndDt', displayName: 'RtOP End Date'},
    { field: 'rtopSeverity', displayName: 'Priority'},
    { field: 'rtopAffectedClients', displayName: 'Client',cellTemplate:$scope.celltemplateClient},
    { field: 'rtopType', displayName: 'RtOP Type'},
    { field: 'rtopStatus', displayName: 'RtOP Status'},
    { field: 'rtopSource', displayName: 'RtOP Source'},
    { field: 'rtopCapability', displayName: 'Capability'},
    { field: 'respOwner', displayName: 'Owner Name', visible:false},
    { field: 'vendor', displayName: 'Vendor', visible:false},
    { field: 'rtopIncidentCreatedDt', displayName: 'Incident Created Date', visible:false},
    { field: 'incidentTicket', displayName: 'Incident #', visible:false},
    { field: 'contactFirstName', displayName: 'Contact First Name', visible:false},
    { field: 'contactLastName', displayName: 'Contact Last Name', visible:false}
    ];

    var opts =  { minHeight: 0 , maxHeight: 500 };
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: 'columnDefs',
        enablePaging: true,
        showFooter: true,
        enableRowSelection: false,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: $scope.mySelections,
        enableColumnResize: true,
        enableColumnReordering: true,
        sortInfo: {fields:['RTOP_ID'], directions:['asc'] },
        footerTemplate : $scope.footerTemplate,
        plugins: [new ngGridFlexibleHeightPlugin(opts)]
    };

    // window resize watcher
    $scope.$watch('rtopColumns', function (newVal, oldVal) {
        if (newVal !== oldVal) {
           $scope.rtopColumns = newVal;
        }
    }, true);

    // user layout watcher
    $scope.$watch('selectedLayout', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.openSide($scope.showNothing,newVal,$scope.rowID);
        }
    }, true);

    // watch window resize when click mini menu
    $scope.$watch('miniMenu', function (newVal, oldVal) {
        if (newVal !== oldVal) {
        $(window).resize();
        }
    }, true);

    // watch paging number
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
    // if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    //watch filter options search text
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

     $scope.$watch('filteredData', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    //should get current month +++
    $scope.showcontent = true;

    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var currentDate = $filter('date')(date, "MM-dd-yyyy");
    var firstDay = $filter('date')(new Date(y, m, 1), "MM-dd-yyyy");
    var lastDay = $filter('date')(new Date(y, m + 1, 0), "MM-dd-yyyy");

    $scope.fromDate = firstDay;
    $scope.untilDate = currentDate;

    //now put a dummy date
    firstDay = '01-01-2014', lastDay = '05-31-2014'
    // console.log(firstDay);
    // console.log(lastDay);
    $scope.filteredData =  [{"rtopID":'',"rtopStartDt":'',"rtopEndDt":'',"rtopSeverity":'',"rtopAffectedClients":'',
                             "rtopType":'',"rtopStatus":'',"rtopSource":'',"rtopCapability":'',"respOwner":'',"vendor":'',
                             "rtopIncidentCreatedDt":'',"incidentTicket":'',"contactFirstName":'',"contactLastName":''}];

    rtopList.query({startdt:firstDay, enddt:lastDay, rtopno:'0', tooluserid:'90', toolmoduleid:'1'}, function(data){
        console.log(data[0].rtopSummary)
        $scope.filteredData = data[0].rtopSummary;
        $scope.tablelist = $scope.gridOptions.$gridScope.columns;
        $scope.getlist();
        $scope.gridOptions.$gridServices.DomUtilityService.RebuildGrid($scope.gridOptions.$gridScope, $scope.gridOptions.ngGrid);
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        console.log($scope.gridOptions.$gridScope);
    });


    }];

});

