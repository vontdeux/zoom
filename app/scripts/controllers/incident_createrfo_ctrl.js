define(['app','directives/timepicker','services/rfoServices','custom/ngGridFlexibleHeightPlugin'], function (app) {
    'use strict';
    return ['$scope','$resource','$http','$filter','$timeout' , 'RFOJsonService' ,
    function ($scope,$resource,$http,$filter,$timeout,RFOJsonService) {
        
    	$scope.rfo = RFOJsonService.getrfo.getData();

        $scope.selected = "";

        $scope.config1 = [];
        $scope.config2 = [];
        $scope.check1 = [];
        $scope.timeline = [];
        $scope.actionplans = [];

        var opts =  { minHeight: 0 , maxHeight: 500 };
        $scope.config1columnDefs = [
            { field: 'ticketnumber', displayName: 'Ticket #'},
            { field: 'start', displayName: 'Start Time'},
            { field: 'end', displayName: 'End Time'}
            ]; 
        $scope.config2columnDefs = [
            { field: 'tickettype', displayName: 'Ticket Type'},
            { field: 'ticketnumber', displayName: 'Ticket #'}
            ];
        $scope.check1columnDefs = [
            { field: 'supplier', displayName: 'Supplier'},
            { field: 'ticketnumber', displayName: 'Ticket #'},
            { field: 'sendcarrier', displayName: 'Send Carrier'},
            { field: 'sendrfo', displayName: 'Send RFO'}
            ];
        $scope.timelinecolumnDefs = [
            { field: 'timeline', displayName: 'Timeline'},
            { field: 'activities', displayName: 'Activities'}
            ];
        $scope.actionplanscolumnDefs = [
            { field: 'description', displayName: 'Description'},
            { field: 'expectation', displayName: 'Expectation'},
            { field: 'actionstatus', displayName: 'Status'},
            { field: 'targetdatetime', displayName: 'Target Date'},
            { field: 'name', displayName: 'Action Owner'},
            { field: 'completiondatetime', displayName: 'Completion Date'}
            ];

        // to resize ng-grid
        $scope.$watch('miniMenu', function (newVal, oldVal) {
          if (newVal !== oldVal) {
            $(window).resize();
            }
        }, true);

        $scope.grid1 = { 
            data: 'config1',
            columnDefs: 'config1columnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]            
        };

        $scope.grid2 = { 
            data: 'config2',
            columnDefs: 'config2columnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]            
        };

        $scope.grid3 = { 
            data: 'check1',
            columnDefs: 'check1columnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]            
        };

        $scope.grid4 = { 
            data: 'timeline',
            columnDefs: 'timelinecolumnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]            
        };

        $scope.grid5 = { 
            data: 'actionplans',
            columnDefs: 'actionplanscolumnDefs',
            enableColumnResize: true,
            showFooter: false,
            plugins: [new ngGridFlexibleHeightPlugin(opts)]            
        };

        $scope.addconfig1 = function(){              
            var obj = angular.copy($scope.selected.config1);                
            $scope.config1.push(obj);            
        };

        $scope.addconfig2 = function(){
            var obj = angular.copy($scope.selected.config2);
            $scope.config2.push(obj);  
        };

        $scope.addcheck1 = function(){
           var obj = angular.copy($scope.selected.check1);
            $scope.check1.push(obj);  
        };

        $scope.addtimeline = function(){
            var obj = angular.copy($scope.selected.timeline);
            $scope.timeline.push(obj);  
        };

        $scope.addkeycontact = function(){
           var obj = angular.copy($scope.selected.keycontacts);
            $scope.keycontacts.push(obj);  
        };

        $scope.addactionplan = function(){
           var obj = angular.copy($scope.selected.actionplans);
            $scope.actionplans.push(obj);  
        };
        
        $scope.selectpanel = function(i){
            $scope.panels.activePanels = [i];
            $scope.rfo.panels[i].isOpen = true;
        };

        $scope.expandall = function(){
            var obj = [];
            var i;            
            
            for(i=0; i < $scope.rfo.panels.length; i++){
                obj.push(i);    
            }                
                        
            $scope.panels.activePanels = obj;
            for(i=0; i < $scope.rfo.panels.length; i++){
                $scope.rfo.panels[i].isOpen = true;
            };
        };

        $scope.collapseall = function(){
            var i;
            $scope.panels.activePanels = [];
            for(i=0; i < $scope.rfo.panels.length; i++){
                $scope.rfo.panels[i].isOpen = false;
            };
        };
        

        $scope.scrolltop = function(){
            $("body").animate({scrollTop: 0}, "fast");
        };
    

        // Tabs list for panels
        // $scope.panels = {activePanels: []};  

        $scope.exports = [
        {
            "text": '<i class="marR10 icon-envelope-o icon-blue"></i>Email',
            "href": "#"
        },
        {
            "text": '<i class="marR10 icon-print icon-blue"></i>Print',
            "click": "#"
        },
        {
        "divider": true
        },
        {
            "text": '<i class="marR10 icon-file-pdf-o icon-blue"></i>Pdf',
            "click": "#"
        },
        {
            "text": '<i class="marR10 icon-file-excel-o icon-blue"></i>Excel',
            "click": "#"
        },
        {
            "text": '<i class="marR10 icon-file-word-o icon-blue"></i>Word',
            "click": "#"
        },
        {
        "divider": true
        },
        {
            "text": '<i class="marR10 icon-star icon-blue"></i>Favourite',
            "click": "#"
        }
        ];

    }];
});
