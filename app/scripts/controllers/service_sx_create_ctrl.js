define(['app','services/restService','directives/incidentDirective','custom/ngGridFlexibleHeightPlugin'], function (app) {
    'use strict';
    return ['$scope','$resource','inFactoryRtop','$http','$filter','$timeout','$state',
    function ($scope,$resource,inFactoryRtop,$http,$filter,$timeout,$state) {

    // dummy selection
    $scope.selectedIcons = [];
        $scope.icons = [
        {value: 'All', label: 'All'},
        {value: 'None', label: 'None'},
        {value: '7-Eleven', label: '7-Eleven'},
        {value: 'AAA', label: 'AAA'},
        {value: 'AAMVA', label: 'AAMVA'},
        {value: 'AAPTLIMITED', label: 'AAPTLIMITED'},
        {value: 'aBBRY', label: 'aBBRY'},
        {value: 'ADEECCO', label: 'AAbA'},
        {value: 'ADIDAS', label: 'AAMVbA'},
        {value: 'AGROSTAR', label: 'AAPT LIMITED'},
        {value: 'AIRGAS', label: 'AIRGAS'},
        {value: 'AHILD', label: 'AHILD'},
        {value: 'ALOHA', label: 'ALOHA'},
        {value: 'AMERUS', label: 'AMERUS'},
        {value: 'ANDHRA', label: 'ANDHRA'},
        {value: 'AAaA', label: 'AAaA'},
        {value: 'AAMVaA', label: 'AAMVaA'},
        {value: 'AMLIM', label: 'AMLIM'}
    ];
    
    }];    
});