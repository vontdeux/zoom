define(['angularAMD'], function (angularAMD) {
	'use strict';
	angularAMD.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('ito', angularAMD.route({
			parent : 'change',
			url: '/ito',
			views: {
				'submenuContent' : angularAMD.route({
					templateUrl: 'views/layout/change-ito.html',
					controllerUrl: 'controllers/change_ito_ctrl'
				})
			}
		}))
		;

    // Else -- This is not working for some reason:
    $urlRouterProvider
    .when('/dashboard', '/dashboard/change');

}]);

});