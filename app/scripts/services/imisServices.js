define(['angularAMD'], function (angularAMD) {
	
	angularAMD.factory('IMISJsonService', function($resource) {
		return {
	      	getimis: $resource('/scripts/data/imis-label.json', {}, {
	        getData: { method: 'GET', params: {} }
	      })
	    };	   
	});		


});