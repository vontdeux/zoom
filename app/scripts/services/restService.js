define(['angularAMD'], function (angularAMD) {
	'use strict';

	// angularAMD.factory("inFactoryRtop", function( $resource ) {
	// 	// http://130.175.209.242:9081/ZOOMService/api/icm/rtop/rtopsummary?startdt=01-01-2014&enddt=05-31-2014&rtopno=0&tooluserid=90&toolmoduleid=1
	// 	return $resource('http://130.175.209.242:9081/ZOOMService/api/icm/rtop/rtopsummary',{}, {
	// 		'getRtop' : {method:'GET',headers: {'Content-Type': 'application/json'},
	// 		            params:{startdt: '@startdt',enddt: '@enddt',rtopno:'@rtopno',tooluserid:'@tooluserid',toolmoduleid:'@toolmoduleid'}, 
	// 		            isArray:true }
	// 	})
	// });

	// angularAMD.factory("inFactoryRtop2", function( $resource ) {
	// 	return $resource('http://130.175.209.242:9081/ZOOMService/api/ICM/RtOP/:id/',{}, {
	// 		'getRtop2' : {method:'GET',headers: {'Content-Type': 'application/json'}, params: {id: '@id'}, isArray:true}
	// 	})
	// });

	// angularAMD.factory("inFactoryRtop3", function( $resource ) {
	// 	return $resource('http://130.175.209.242:9081/ZOOMService/api/icm/master/1/1',{}, {
	// 		'getData' : {method:'GET',headers: {'Content-Type': 'application/json'}, params: {}, isArray:true}
	// 	})
	// });

	angularAMD.factory('incidentMasterData', ['$resource',
		function($resource){
			return $resource('http://130.175.209.242:9081/ZOOMService/api/icm/master/1/1', {}, {
			query: {method:'GET',headers: {'Content-Type': 'application/json'}, isArray:true}
		});
	}]);

	angularAMD.factory('rtopSelectbox', ['$resource',
		function($resource){
			return $resource('http://130.175.209.242:9081/ZOOMService/api/icm/master/1/1', {}, {
			query: {method:'GET',headers: {'Content-Type': 'application/json'}, isArray:true}
		});
	}]);

	angularAMD.factory('rtopList', ['$resource',
		function($resource){
			return $resource('http://130.175.209.242:9081/ZOOMService/api/icm/rtop/rtopsummary', {}, {
			query: {method:'GET',headers: {'Content-Type': 'application/json'},
			        params:{startdt: '@startdt',enddt: '@enddt',rtopno:'@rtopno',tooluserid:'@tooluserid',toolmoduleid:'@toolmoduleid'}, 
			        isArray:true }
		});
	}]);

	angularAMD.factory('inFactoryRtop', ['$resource',
		function($resource){
			return $resource('http://130.175.209.242:9081/ZOOMService/api/icm/rtop/rtopsummary', {}, {
			query: {method:'GET',headers: {'Content-Type': 'application/json'},
			        params:{startdt: '@startdt',enddt: '@enddt',rtopno:'@rtopno',tooluserid:'@tooluserid',toolmoduleid:'@toolmoduleid'}, 
			        isArray:true }
		});
	}]);

});