define(['angularAMD'], function (angularAMD) {
	
	angularAMD.factory('RFOJsonService', function($resource) {
		return {
	      	getrfo: $resource('/scripts/data/rfo-label.json', {}, {
	        getData: { method: 'GET', params: {} }
	      })
	    };	   
	});		


});