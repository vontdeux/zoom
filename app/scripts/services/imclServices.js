define(['angularAMD'], function (angularAMD) {
	
	angularAMD.factory('IMCLJsonService', function($resource) {
		return {
	      	getimcl: $resource('/scripts/data/imcl-label.json', {}, {
	        getData: { method: 'GET', params: {} }
	      })
	    };	   
	});		


});