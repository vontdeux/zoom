angular.module("template-override", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("template/select/incident-rtop_view_by.html",
   '<ul tabindex="-1" class="select dropdown-menu mtp" ng-show="$isVisible()" role="select">\n '+
	  	'<li ng-if="$showAllNoneButtons">\n '+
		  	'<div class="btn-group" style="margin-bottom: 5px; margin-left: 5px">\n '+
		  		'<button type="button" class="btn btn-primary btn-xs" ng-click="$selectAll()">{{$allText}}</button>\n '+ 
		  		'<button type="button" class="btn btn-primary btn-xs" ng-click="$selectNone()">{{$noneText}}</button>\n '+
		  	'</div>\n '+
	  	'</li>\n '+
	  	'<li role="presentation" ng-repeat="match in $matches" ng-class="{active: $isActive($index)}">\n '+
	  	 	'<a style="cursor: default" role="menuitem" tabindex="-1" ng-click="$select($index, $event);openMe($index);">\n '+
	  		'<i class="{{$iconCheckmark}}" ng-if="$isMultiple && $isActive($index)"></i>\n '+ 
	  		'<span ng-bind="match.label"></span>\n '+
	  		'</a>\n '+
	  	'</li>\n '+
  	'</ul>');

  $templateCache.put("template/popover/incident-grid.html",
   '<div class="popover">\n '+
	   '<div class="arrow"></div>\n '+
	   '<h3 class="popover-title">RTOP #: {{row.entity.rtopID}}\n '+
	   '</h3> \n '+
	   '<div class="popover-content">\n '+
	   '<ul>\n '+
	   	'<li>\n '+
	   		'rtopStartDt : {{row.entity.rtopStartDt}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'rtopEndDt : {{row.entity.rtopEndDt}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'rtopSeverity : {{row.entity.rtopSeverity}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'rtopAffectedClients : {{row.entity.rtopAffectedClients}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'rtopType : {{row.entity.rtopType}}\n '+
	   	'</li>\n '+
	   	'<li class="divider"></li>\n '+
	   	'<li>\n '+
	   		'rtopStatus : {{row.entity.rtopStatus}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'rtopSource: {{row.entity.rtopSource}}\n '+
	   	'</li>\n '+
	   	'<li class="divider"></li>\n '+
	   	'<li>\n '+
	   		'rtopCapability : {{row.entity.rtopCapability}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'respOwner : {{row.entity.respOwner}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'vendor : {{row.entity.vendor}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'rtopIncidentCreatedDt : {{row.entity.rtopIncidentCreatedDt}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'incidentTicket : {{row.entity.incidentTicket}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'contactFirstName : {{row.entity.contactFirstName}}\n '+
	   	'</li>\n '+
	   	'<li>\n '+
	   		'contactLastName : {{row.entity.contactLastName}}\n '+
	   	'</li>\n '+
	   	// '<li class="divider"></li>\n '+
	   	// '<li>\n '+
	   	// 	'<a data-ui-sref=\"rtopDetail({rtopID : row.entity.RTOP_ID})\" ng-click=\"openSide(\'open\')\">View Details</a>\n '+
	   	// '</li>\n '+
	   '</ul>\n '+
	   '</div>\n '+
	'</div>');

	$templateCache.put("template/select/default-select.html",
	   '<ul tabindex="-1" class="select dropdown-menu mtp" ng-show="$isVisible()" role="select">\n '+
	   '<li ng-if="$showAllNoneButtons">\n '+
	   '<div class="btn-group" style="width:100%;padding:3px 20px;">\n '+
	   '<button type="button" style="width:50%;border-right: 2px solid #ddd;border-bottom: 1px solid #ddd;" class="btn btn-default btn-sm btn-all" ng-click="$selectAll()">{{$allText}}</button> \n '+
	   '<button type="button" style="width:50%;border-bottom: 1px solid #ddd;" class="btn btn-default btn-sm btn-none" ng-click="$selectNone()">{{$noneText}}</button>\n '+
	   '</div></li><li role="presentation" ng-repeat="match in $matches" ng-class="{active: $isActive($index)}">\n '+
	   '<a style="cursor: default" role="menuitem" tabindex="-1" ng-click="$select($index, $event)">\n '+
	   '<i class="{{$iconCheckmark}}" ng-if="$isMultiple && $isActive($index)"></i> <span ng-bind="match.label">\n '+
	   '</span></a></li></ul>');

	$templateCache.put("template/modal/email.html",
	   '<div class="modal" tabindex="-1" role="dialog" aria-hidden="true">'+
		  '<div class="modal-dialog">'+
		    '<div class="modal-content">'+
		      '<div class="modal-header">'+
		        '<button type="button" class="close" aria-label="Close" ng-click="$hide()"><span aria-hidden="true">&times;</span></button>'+
		        '<h4 class="modal-title">Send Email</h4>'+
		      '</div>'+
		      '<div class="modal-body">'+
			    '<div class="row">'+
			        '<div class="col-sm-3"><button class="btn-hp btn-primary">To</button></div>'+
			        '<div class="col-sm-9"><input type="text" class="form-control"></div>'+
			    '</div>'+
			    '<div class="row">'+
			        '<div class="col-sm-3"><button class="btn-hp btn-primary">Cc</button></div>'+
			        '<div class="col-sm-9"><input type="text" class="form-control"></div>'+
			    '</div>'+
			    '<div class="row">'+
			        '<div class="col-sm-3"><button class="btn-hp btn-primary">Bcc</button></div>'+
			        '<div class="col-sm-9"><input type="text" class="form-control"></div>'+
			    '</div>'+
			    '<div class="row">'+
			        '<div class="col-sm-12"><label><input type="checkbox" value="0"><span>Send me a copy</span></label></div>'+
			    '</div>'+
			    '<div class="row">'+
			       '<div class="col-sm-3">Subject</div>'+
			        '<div class="col-sm-9"><input type="text" class="form-control"></div>'+
			    '</div>'+
			    '<div class="row">'+
			        '<div class="col-sm-12"><textarea class="form-control"></textarea></div>'+
			    '</div>'+
			    '<div class="row">'+
			        '<div class="col-sm-12"><label><input type="checkbox" value="0"><span>Include Attachments</span></label></div>'+
			    '</div>'+
		      '</div>'+
		      '<div class="modal-footer">'+
		        '<button type="button" class="btn btn-default" ng-click="$hide()">Close</button>'+
		      '</div>'+
		  '</div>'+
		'</div>');

$templateCache.put('dropdown/custom-dropdown.tpl.html', 
'<ul tabindex="-1" class="dropdown-menu" role="menu">'+
    '<li role="presentation">'+
        '<a role="menuitem" tabindex="-1" class="" ng-click="openItem(\'email\')">'+
            '<i class="marR10 icon-envelope-o icon-blue"></i>Email'+
        '</a>'+
    '</li>'+
    '<li role="presentation">'+
        '<a role="menuitem" tabindex="-1" class="">'+
            '<i class="marR10 icon-print icon-blue"></i>Print'+
        '</a>'+
    '</li>'+
    '<li role="presentation" class="ng-scope divider">'+
    '</li>'+
    '<li role="presentation">'+
        '<a role="menuitem" tabindex="-1" class="">'+
            '<i class="marR10 icon-file-pdf-o icon-blue"></i>Pdf'+
        '</a>'+
    '</li>'+
    '<li role="presentation">'+
        '<a role="menuitem" tabindex="-1" class="">'+
            '<i class="marR10 icon-file-excel-o icon-blue"></i>Excel'+
        '</a>'+
    '</li>'+
    '<li role="presentation">'+
        '<a role="menuitem" tabindex="-1" class="">'+
            '<i class="marR10 icon-file-word-o icon-blue"></i>Word'+
        '</a>'+
    '</li>'+
    '<li role="presentation" class="ng-scope divider">'+
    '</li>'+
    '<li role="presentation">'+
        '<a role="menuitem" tabindex="-1" class="">'+
            '<i class="marR10 icon-star icon-blue"></i>Favourite'+
        '</a>'+
    '</li>'+
'</ul>');


}]);